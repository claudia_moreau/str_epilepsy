
################################
#
#Claudia Moreau
#
#7 dec 2018
#
#MAke stats for SV results
#
################################


rm(list = ls());
library(matrixTests)
library(stringr)
library(tidyr)
library(erer)

args <- commandArgs(trailingOnly = TRUE)
output_file<-args[1] ## outputfile prefix
seuil<-as.numeric(args[2]) ## IQR threshold above q75

## Put your work dir here
work_dir <- ''
setwd(work_dir)
missing_tresh<-52 ## Missing threshold badly hard coded here at 2% of alleles

## Phenotype data
epi_pheno_file<-paste('data/epilepsy_phenos.txt',sep='')
pheno_df<-read.table(epi_pheno_file,fill=TRUE,colClasses='character')
pheno_df[is.na(pheno_df[,2]),2] <- 'UNE'
dimnames(epi_pheno_df)[[2]] <- c("sample","pheno")

ichr<-1
## All genes data
total_tred_str_file<-paste('all_samples_rep.chr',ichr,'.no_centro_telo_low_map.txt',sep="")
## epilepsy genes data
# total_tred_str_file<-paste('all_samples_EE_parents_ADNI_allepi_strs_rep.no_centro_telo_low_map.txt',sep="")

total_tred_df <- read.table(total_tred_str_file,header=TRUE,row.names=1)

###### Don't do this part for epilepsy genes #######
for (ichr in c(2:22,'1_2','2_2','3_2','4_2','5_2','6_2','X')) {
	tmp_total_tred_str_file<-paste('all_samples_rep.chr',ichr,'.no_centro_telo_low_map.txt',sep="")		
	tmp_total_tred_df <- read.table(tmp_total_tred_str_file,header=TRUE,row.names=1)
	total_tred_df <- rbind(total_tred_df,tmp_total_tred_df)	
}
##### end part #######

total_tred_df <- total_tred_df[order(dimnames(total_tred_df)[[1]]),]

## Remove strs with more than xx missing values (missing = -1)
total_tred_df$missing<-apply(total_tred_df,1,function(x) length(x[x<0])) ## Missing value = -1
total_tred_df <- total_tred_df[total_tred_df$missing<=missing_tresh,]
total_tred_df[total_tred_df==-1] <- NA

## Make vectors of samples
patients_samples_tmp <- pheno_df$sample[pheno_df$pheno=='CAE' | pheno_df$pheno=='JAE' |pheno_df$pheno=='JME' |pheno_df$pheno=='GTCS' | pheno_df$pheno=='UNG' | pheno_df$pheno=='HS' | pheno_df$pheno=='EPNL' |pheno_df$pheno=='EPLSSH' |pheno_df$pheno=='UNF' | pheno_df$pheno=='UNE' | pheno_df$pheno=='JVONS' |pheno_df$pheno=='EE']
patients_samples<-c(paste(patients_samples_tmp,'1',sep='_'),paste(patients_samples_tmp,'2',sep='_'))

ctrls_samples_tmp <- pheno_df$sample[pheno_df$pheno=='EE_parents' ]
ctrls_samples<-str_replace(c(paste(ctrls_samples_tmp,'1',sep='_'),paste(ctrls_samples_tmp,'2',sep='_')),'-','.')

adni_samples_tmp <- pheno_df$sample[pheno_df$pheno=='ADNI' ]
adni_samples<-str_replace(c(paste(adni_samples_tmp,'1',sep='_'),paste(adni_samples_tmp,'2',sep='_')),'-','.')

## Calculate IQR only in ctrls
iqr_df<-total_tred_df[,is.element(dimnames(total_tred_df)[[2]], ctrls_samples) | is.element(dimnames(total_tred_df)[[2]], adni_samples)]
iqr_df$iqr <- apply(iqr_df,1,function(x) (IQR(x,na.rm=TRUE)))
iqr_df$q75 <- apply(iqr_df[,-length(iqr_df[1,])],1,function(x) (quantile(x,na.rm=TRUE)))[4,]

## Calculate IQR in patients
iqr_pat_df<-total_tred_df[,is.element(dimnames(total_tred_df)[[2]], patients_samples) ]

## Look at patient alleles IQR above q75 
pat_IQR_lst=list()
for (i in c(1:length(iqr_pat_df[1,]))){
	pat_IQR_lst[[i]] <- (iqr_pat_df[,i] - iqr_df$q75) / iqr_df$iqr
	pat_IQR_lst[[i]][iqr_df$iqr==0] <- (iqr_pat_df[,i][iqr_df$iqr==0] - iqr_df$q75[iqr_df$iqr==0]) 
}

iqr_pat_mat<-as.data.frame(matrix(unlist(pat_IQR_lst),nrow=length(iqr_df[,1])))
dimnames(iqr_pat_mat)[[1]] <- dimnames(iqr_df)[[1]]
dimnames(iqr_pat_mat)[[2]] <- dimnames(iqr_pat_df)[[2]]
iqr_pat_mat$max<-apply(iqr_pat_mat,1,function (x) (max(x[!is.na(x)])))


## Apply IQR threshold
seuil_tred_df<-total_tred_df[is.element(dimnames(total_tred_df)[[1]],dimnames(iqr_pat_mat)[[1]][iqr_pat_mat$max>seuil]),is.element(dimnames(total_tred_df)[[2]], ctrls_samples) | is.element(dimnames(total_tred_df)[[2]], patients_samples)  | is.element(dimnames(total_tred_df)[[2]], adni_samples)]

## transform data
rep_df<-seuil_tred_df %>% gather(sample,rep,dimnames(seuil_tred_df)[[2]][1]:dimnames(seuil_tred_df)[[2]][length(seuil_tred_df[1,])])
rep_df$str<-as.factor((rep(dimnames(seuil_tred_df)[[1]],length(seuil_tred_df[1,]))))
tmp_vec<-rep ('NA',length(rep_df[,1]))
tmp_vec[is.element(rep_df$sample,patients_samples)] <- 'cases'
tmp_vec[is.element(rep_df$sample,ctrls_samples)] <- 'controls'
tmp_vec[is.element(rep_df$sample,adni_samples)] <- 'ADNI_controls'
rep_df$group<-as.factor(tmp_vec)

max_df<-merge (as.data.frame(aggregate(rep~str, rep_df[rep_df$group=='cases',], FUN=max)),as.data.frame(aggregate(rep~str, rep_df[rep_df$group!='cases',], FUN=max)),by='str')
max_df$diff<-max_df[,3]/max_df[,2]

## Threshold of control / case
seuil_max<-0.6

rep_df<-rep_df[max_df$diff<seuil_max,]
rep_df$str<-factor(rep_df$str)

myfile <- paste(output_file,'_missing',missing_tresh,'_iqr_thresh',seuil,'_maxrepCaseCtrl',seuil_max,'_single.highest.IQR.str.rep.table.txt',sep="")
write.table(rep_df,myfile,quote=FALSE,row.names=FALSE)

## resume table 4 article
resume_table<-xtabs(rep~str+group,aggregate(rep~str+group,rep_df,max))
myfile <- paste(output_file,'_missing',missing_tresh,'_iqr_thresh',seuil,'_maxrepCaseCtrl',seuil_max,'_single.highest.IQR.str.rep.table.resume.txt',sep="")
write.table(resume_table,myfile,quote=FALSE)


q()



