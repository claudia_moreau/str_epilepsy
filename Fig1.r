
################################
#
# Claudia Moreau
#
# 05-08-2020
#
# MAke Fig1 for paper STRs in epilepsy
#
################################


rm(list = ls());

# Put your work directory here
work_dir <- ''
setwd(work_dir)

neurodev_strs_file<-'data/repeat_expansion_disorders_aff_alleles_4article.txt'
# File with 6 coloumns: 
# Source  locus norm_low  norm_up aff_low aff_up
# Balho DRPLA 7 34  49  88
neurodev_strs_df <- read.table(neurodev_strs_file,colClasses=c(rep("character",2),rep("numeric",4)),header=TRUE)

str_files <- neurodev_strs_df[,2]
str_thres <- neurodev_strs_df[,5]

## Alleles above thresholds
positive_df <- data.frame(str=c(str_files,str_files,str_files),str_factor=c(as.factor(str_files),as.factor(str_files),as.factor(str_files)),group=c(rep('controls',length(str_files)),rep('EE',length(str_files)),rep('epilepsies',length(str_files))),group_factor=c(rep(1,length(str_files)),rep(2,length(str_files)),rep(3,length(str_files))),col=as.character(c(rep('darkgrey',length(str_files)),rep('#66CC66',length(str_files)),rep('#6b8915',length(str_files)))),seuil=c(str_thres,str_thres,str_thres))
positive_df$xpos<-((positive_df$str_factor-1)*3)+positive_df$group_factor


## Phenotype data
epi_pheno_file<-'data/epilepsy_phenos.txt'
## File with 4 coloumns: 
# "sample" "pheno"
#  S01542  CAE
epi_pheno_df<-read.table(epi_pheno_file,fill=TRUE)
epi_pheno_df[is.na(epi_pheno_df[,2]),2] <- 'UNE'
dimnames(epi_pheno_df)[[2]] <- c("sample","pheno")


## TREDPARSE STRs
i=1
str_file<-paste('results/',str_files[i],'.samples.rep.3.txt',sep="")
## file with 4 coloumns:
# batch sample allele1 allele2
# batch10 LP6005115-DNA_A02 2 6
str_tmp_df <- read.table(str_file,colClasses=c("character","character","numeric","numeric"))

## Make results table
str_df <- data.frame(sample=c(str_tmp_df[!duplicated(str_tmp_df[,2]),2],str_tmp_df[!duplicated(str_tmp_df[,2]),2]),rep=c(str_tmp_df[!duplicated(str_tmp_df[,2]),3],str_tmp_df[!duplicated(str_tmp_df[,2]),4]))
str_df$str <- (rep(str_files[i],length(str_df[,1])))
str_df$pheno <- epi_pheno_df$pheno[match(str_df$sample,epi_pheno_df$sample)]

for (i in c(2:length(str_files))) {

	str_file<-paste('results/TREDPARSE/',str_files[i],'.samples.rep.3.txt',sep="")

	if (file.exists(str_file)){
		print (str_file)

		str_tmp_df <- read.table(str_file,colClasses=c("character","character","numeric","numeric"))
		str_tmp_2_df <- data.frame(sample=c(str_tmp_df[!duplicated(str_tmp_df[,2]),2],str_tmp_df[!duplicated(str_tmp_df[,2]),2]),rep=c(str_tmp_df[!duplicated(str_tmp_df[,2]),3],str_tmp_df[!duplicated(str_tmp_df[,2]),4]))
		str_tmp_2_df$str <-  (rep(str_files[i],length(str_tmp_2_df[,1])))
		str_tmp_2_df$pheno <- epi_pheno_df$pheno[match(str_tmp_2_df$sample,epi_pheno_df$sample)]
		str_df<-rbind(str_df,str_tmp_2_df)

	}
}

## Define groups
str_df$group<- (rep('NA',length(str_df$sample)))
str_df$group[str_df$pheno=='CAE' | str_df$pheno=='JAE' |str_df$pheno=='JME' |str_df$pheno=='GTCS' | str_df$pheno=='UNG' | str_df$pheno=='HS' | str_df$pheno=='EPNL' |str_df$pheno=='EPLSSH' |str_df$pheno=='UNF' | str_df$pheno=='UNE' | str_df$pheno=='JVONS' ] <- 'epilepsies'
str_df$group[str_df$pheno=='EE' ] <- 'EE'
str_df$group[str_df$pheno=='ADNI' | str_df$pheno=='EE_parents'] <- 'controls'
str_df <- str_df[str_df$group != 'NA',]
str_df$group<-as.factor(str_df$group)
str_df$str<-as.factor(str_df$str)
str_df$rep[str_df$rep==-1] <- NA

## Find alleles above thresholds
ypos=c()
xpos=c()
dot_col<-c()
samples_vec<-c()
strs_vec=c()
group_vec=c()
cols<-c('pink','red','red')

for (i in c(1:length(positive_df[,1]))){
  pos=(str_df$rep[str_df$str==positive_df$str[i] & str_df$group==positive_df$group[i] & str_df$rep>=positive_df$seuil[i]])

  if (length(pos)>0){
    ypos<-c(ypos,pos)
    xpos<-c(xpos,rep(positive_df$xpos[i],length(pos)))
    dot_col<-c(dot_col,rep(cols[positive_df$group_factor[i]],length(pos)))
    samples_vec <- c(samples_vec,(str_df$sample[(str_df$str==positive_df$str[i] & str_df$group==positive_df$group[i] & str_df$rep>=positive_df$seuil[i])]))
    strs_vec <- c(strs_vec,(str_df$str[(str_df$str==positive_df$str[i] & str_df$group==positive_df$group[i] & str_df$rep>=positive_df$seuil[i])]))
    group_vec <- c(group_vec,(str_df$group[(str_df$str==positive_df$str[i] & str_df$group==positive_df$group[i] & str_df$rep>=positive_df$seuil[i])]))

  }
}


## Make boxplots
myfile <- paste('results/Fig1.jpg',sep="")

jpeg(filename=myfile,width=20,height=8.5,res=300,unit="in")
par(mar=c(7,6,2,2))
boxplot(rep~group+str,data=str_df,col=rep(c('darkgrey','#66CC66','#6b8915'),length(levels(str_df$str))),pars=list(outcol=rep(c('darkgrey','#66CC66','#6b8915'),length(levels(str_df$str)))),xlab="",ylab="",main='',cex.lab=2, axes=FALSE)
mtext("Number of repetitions" ,side = 2, line = 4,cex=2)
legend('topleft', legend=levels(str_df$group), col =c('darkgrey','#66CC66','#6b8915') , cex=2,bty='n',pch=15)
axis(2,at=seq(from=0,to=200,by=25),labels=seq(from=0,to=200,by=25),cex.axis=2,las=2)
axis(1,at=seq(2,length(levels(str_df$str))*length(levels(str_df$group))-1,length(levels(str_df$group))),labels=levels(str_df$str),cex.axis=2,las=2,lwd=0, lwd.ticks=0)
points(x=xpos,y=ypos,col=dot_col,pch=19)

dev.off()

q()



