
################################
#
# Claudia Moreau
#
# 05-08-2020
#
# MAke Fig3 for paper STRs in epilepsy
#
################################

rm(list = ls());

library("stringi")
library(stringr)
library(RColorBrewer)
brewer.pal(9, 'Reds')

## Put your work dir here
work_dir <- ''
setwd(work_dir)
pedigree_dir<-'data/'
missing_tresh<-0

## Pheno data
twin_pheno_file<-paste(pedigree_dir,'twins_pedigree.txt',sep='')
## File with 7 coloumns:
# sample dataset famID sample_again fam_member dont_know_what_that_is
# LP6005058-DNA_A02	Twins	1490	R0026057	Mother	1	DISK_3

twin_pheno_df<-read.table(twin_pheno_file,colClasses='character')
twin_fam_df<-data.frame(fam=twin_pheno_df[!duplicated(twin_pheno_df[,3]),3])
twin_fam_df$twin1 <-twin_pheno_df[match(twin_fam_df$fam,twin_pheno_df[,3]) & twin_pheno_df[,5] == 'Twin1',1]
twin_fam_df$twin2 <-twin_pheno_df[match(twin_fam_df$fam,twin_pheno_df[,3]) & twin_pheno_df[,5] == 'Twin2',1]

## Read in clean TREDPARSE restults
str_vec<-c()
same_vec<-c()
same_pm_vec<-c()
same_pm2_vec<-c()
ref_legnth_vec<-c()
mean_vec<-c()

for (i in c(1:22,'1_2','2_2','3_2','4_2','5_2','6_2','X')){
	str_file<-paste('results/twins_rep.chr',i,'.no_centro_telo_low_map.txt',sep="")
	str_tmp_df <- read.table(str_file,header=TRUE,row.names=1)

	ref_file<-paste('results/chr',i,'.ref.txt',sep="")
	ref_df<-read.table(ref_file,colClasses=c("character","character","numeric"))
	ref_df$motif_length<-stri_length(ref_df[,2])
	str_tmp_df$missing<-apply(str_tmp_df,1,function(x) length(x[x<0])) ## missing value = -1
	str_tmp_df <- str_tmp_df[str_tmp_df$missing<=missing_tresh,]
	str_tmp_df$motif_length <- ref_df$motif_length[match(dimnames(str_tmp_df)[[1]],ref_df[,1])]
	str_tmp_df$ref_length <- ref_df[match(dimnames(str_tmp_df)[[1]],ref_df[,1]),3] * str_tmp_df$motif_length
	ref_legnth_vec=c(ref_legnth_vec,str_tmp_df$ref_length)

	twin1_df<-str_tmp_df[,match(str_replace(c(paste(twin_fam_df$twin1,'_1',sep=''),paste(twin_fam_df$twin1,'_2',sep='')),'-','.'),dimnames(str_tmp_df)[[2]]) ]*str_tmp_df$motif_length
	twin2_df<-str_tmp_df[,match(str_replace(c(paste(twin_fam_df$twin2,'_1',sep=''),paste(twin_fam_df$twin2,'_2',sep='')),'-','.'),dimnames(str_tmp_df)[[2]]) ]*str_tmp_df$motif_length

	str_vec<-c(str_vec,dimnames(twin1_df)[[1]])

	same_df<-rep(0,length(twin1_df[,1]))
	same_df[twin1_df[,1]==twin2_df[,1]]<-1
	for (j in c(2:20)){
		same_tmp_vec<-rep(0,length(twin1_df[,1]))
		same_tmp_vec[twin1_df[,j]==twin2_df[,j]]<-1
		same_df<-cbind(same_df,same_tmp_vec)
	}
	same_vec<- c(same_vec,apply(same_df,1,sum))

	## +- 1 rep
	for (j in c(1:20)){
		same_df[twin1_df[,j]==twin2_df[,j]+(1*str_tmp_df$motif_length) | twin1_df[,j]==twin2_df[,j]-(1*str_tmp_df$motif_length),j]<-1
	}
	same_pm_vec<- c(same_pm_vec,apply(same_df,1,sum))
	
	## +- 2rep
	for (j in c(1:20)){
		same_df[twin1_df[,j]==twin2_df[,j]+(2*str_tmp_df$motif_length) | twin1_df[,j]==twin2_df[,j]-(2*str_tmp_df$motif_length),j]<-1
	}
	same_pm2_vec<- c(same_pm2_vec,apply(same_df,1,sum)	)
	mean_vec<-c(mean_vec,apply(cbind(twin1_df,twin2_df),1,mean))

}


## Make figure
length_col <- 5
log_seq<-c(1,10,100,1000,10000,100000)

my_table<-data.frame(str=str_vec,mean_length=round(mean_vec),ref_length=ref_legnth_vec,exact_same=same_vec,approx1_same=same_pm_vec,approx2_same=same_pm2_vec)


myfile <- 'results/ttest/twins.strs.comp.jpg'
jpeg(filename=myfile,width=8.5,height=11,res=300,unit="in")
par(mar=c(5,6,5,6),mfrow=c(3,1))

count_total<-as.data.frame(table(my_table$mean_length))

mean_same<-as.data.frame(table(my_table$mean_length,my_table$approx2_same))
mean_same<-mean_same[mean_same$Freq>0,]
mean_same$total<-count_total$Freq[match(mean_same$Var1,count_total$Var1)]
mean_same$ratio<-rep (0,length(mean_same$Freq))
for (l in c(1:length(log_seq)-1)){
	mean_same$ratio[mean_same$Freq>=log_seq[l] & mean_same$Freq<log_seq[l+1]]<-l
}
mean_same$num_var1<-as.numeric(levels(mean_same$Var1))[mean_same$Var1]
mean_same$num_var2<-as.numeric(levels(mean_same$Var2))[mean_same$Var2]

tmp_col_vec<-rep(0,length(my_table[,1]))
for (k in c(1:length(mean_same[,1]))){
	tmp_meaan<-mean_same$num_var1[k]
	tmp_same<-mean_same$num_var2[k]
	tmp_ratio<-mean_same$ratio[k]

	tmp_col_vec[my_table$mean_length==tmp_meaan & my_table$approx2_same==tmp_same] <- tmp_ratio
}

col_vec<-brewer.pal(length_col, 'Reds')[tmp_col_vec]

count_20<-as.data.frame(table((my_table$mean_length)[my_table$approx2_same==20],my_table$approx2_same[my_table$approx2_same==20]))
count_20$total<-count_total$Freq[match(count_20$Var1,count_total$Var1)]
plot(x=my_table$mean_length,y=my_table$approx2_same,pch=15,cex=2,col=col_vec,cex.axis=2,cex.lab=2,xlab='',ylab='',main='Twin alleles match ± 2 repeats',cex.main=3)
par(new = TRUE)
plot(x=as.numeric(levels(count_20$Var1))[count_20$Var1],y=count_20$Freq/count_20$total, type = "l", xaxt = "n", yaxt = "n",ylab = "", xlab = "", col = "black", lty = 1,cex.axis=2)
axis(side = 4,cex.axis=2)
legend('bottomleft',pch=15,col=brewer.pal(length_col, 'Reds'),legend=paste(log_seq[-6],log_seq[-1],sep='-'))
mtext('A',3,cex=3,adj=0,line=0.5)

mean_same<-as.data.frame(table(my_table$mean_length,my_table$approx1_same))
mean_same<-mean_same[mean_same$Freq>0,]
mean_same$total<-count_total$Freq[match(mean_same$Var1,count_total$Var1)]
mean_same$ratio<-rep (0,length(mean_same$Freq))
for (l in c(1:length(log_seq)-1)){
	mean_same$ratio[mean_same$Freq>=log_seq[l] & mean_same$Freq<log_seq[l+1]]<-l
}
mean_same$num_var1<-as.numeric(levels(mean_same$Var1))[mean_same$Var1]
mean_same$num_var2<-as.numeric(levels(mean_same$Var2))[mean_same$Var2]


tmp_col_vec<-rep(0,length(my_table[,1]))
for (k in c(1:length(mean_same[,1]))){
	tmp_meaan<-mean_same$num_var1[k]
	tmp_same<-mean_same$num_var2[k]
	tmp_ratio<-mean_same$ratio[k]

	tmp_col_vec[my_table$mean_length==tmp_meaan & my_table$approx1_same==tmp_same] <- tmp_ratio
}


col_vec<-brewer.pal(length_col, 'Reds')[tmp_col_vec]
count_20<-as.data.frame(table((my_table$mean_length)[my_table$approx1_same==20],my_table$approx1_same[my_table$approx1_same==20]))
count_20$total<-count_total$Freq[match(count_20$Var1,count_total$Var1)]
plot(x=my_table$mean_length,y=my_table$approx1_same,pch=15,cex=2,col=col_vec,cex.axis=2,cex.lab=3,xlab='',ylab='',main='Twin alleles match ± 1 repeat',cex.main=3)
par(new = TRUE)
plot(x=as.numeric(levels(count_20$Var1))[count_20$Var1],y=count_20$Freq/count_20$total, type = "l", xaxt = "n", yaxt = "n",ylab = "", xlab = "", col = "black", lty = 1,cex.axis=2)
axis(side = 4,cex.axis=2)
mtext("Proportion of STRs where matches = 20 / total STRs in length bin", side = 4, line = 4,cex=2)
mtext("Number of twin alleles matching", side = 2, line = 3.5,cex=2)
mtext('B',3,cex=3,adj=0,line=0.5)


mean_same<-as.data.frame(table(my_table$mean_length,my_table$exact_same))
mean_same<-mean_same[mean_same$Freq>0,]
mean_same$total<-count_total$Freq[match(mean_same$Var1,count_total$Var1)]

mean_same$ratio<-rep (0,length(mean_same$Freq))
for (l in c(1:length(log_seq)-1)){
	mean_same$ratio[mean_same$Freq>=log_seq[l] & mean_same$Freq<log_seq[l+1]]<-l
}

mean_same$num_var1<-as.numeric(levels(mean_same$Var1))[mean_same$Var1]
mean_same$num_var2<-as.numeric(levels(mean_same$Var2))[mean_same$Var2]

tmp_col_vec<-rep(0,length(my_table[,1]))
for (k in c(1:length(mean_same[,1]))){
	tmp_meaan<-mean_same$num_var1[k]
	tmp_same<-mean_same$num_var2[k]
	tmp_ratio<-mean_same$ratio[k]

	tmp_col_vec[my_table$mean_length==tmp_meaan & my_table$exact_same==tmp_same] <- tmp_ratio
}


col_vec<-brewer.pal(length_col, 'Reds')[tmp_col_vec]
count_20<-as.data.frame(table((my_table$mean_length)[my_table$exact_same==20],my_table$exact_same[my_table$exact_same==20]))
count_20$total<-count_total$Freq[match(count_20$Var1,count_total$Var1)]
plot(x=my_table$mean_length,y=my_table$exact_same,pch=15,cex=2,col=col_vec,cex.axis=2,cex.lab=3,xlab='Mean STR length of the 20 twin alleles',ylab='',main='Twin alleles match exact same repeat number',cex.main=2.5)
par(new = TRUE)
plot(x=as.numeric(levels(count_20$Var1))[count_20$Var1],y=count_20$Freq/count_20$total, type = "l", xaxt = "n", yaxt = "n",ylab = "", xlab = "", col = "black", lty = 1,cex.axis=2)
axis(side = 4,cex.axis=2)
mtext('C',3,cex=3,adj=0,line=0.5)
dev.off()


q()



