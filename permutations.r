
################################
#
#Claudia Moreau
#
#7 dec 2018
#
#MAke stats for SV results
#
################################


rm(list = ls());
library(matrixTests)
library(stringr)
library(tidyr)
library(erer)


args <- commandArgs(trailingOnly = TRUE)
output_file<-args[1] ## outputfile prefix
seuil<-as.numeric(args[2])

# Put your work directory here
work_dir <- ''
setwd(work_dir)
missing_tresh<-52



epi_pheno_file<-'data/epilepsy_phenos.txt'
epi_pheno_df<-read.table(epi_pheno_file,fill=TRUE,colClasses='character')
epi_pheno_df[is.na(epi_pheno_df[,2]),2] <- 'UNE'
dimnames(epi_pheno_df)[[2]] <- c("sample","pheno")

## Load real data
total_tred_str_file<-paste('all_samples_EE_parents_ADNI_allepi_strs_rep.no_centro_telo_low_map.txt',sep="")
total_tred_df <- read.table(total_tred_str_file,header=TRUE,row.names=1)


# total_tred_df[1:10,1:10]
total_tred_df <- total_tred_df[order(dimnames(total_tred_df)[[1]]),]
total_tred_df$missing<-apply(total_tred_df,1,function(x) length(x[x<0]))
total_tred_df <- total_tred_df[total_tred_df$missing<=missing_tresh,]
total_tred_df[total_tred_df==-1] <- NA

## Make vectors of samples
patients_samples_tmp <- pheno_df$sample[!is.na(pheno_df$sample) & (pheno_df$pheno=='CAE' | pheno_df$pheno=='JAE' |pheno_df$pheno=='JME' |pheno_df$pheno=='GTCS' | pheno_df$pheno=='UNG' | pheno_df$pheno=='HS' | pheno_df$pheno=='EPNL' |pheno_df$pheno=='EPLSSH' |pheno_df$pheno=='UNF' | pheno_df$pheno=='UNE' | pheno_df$pheno=='JVONS' |pheno_df$pheno=='EE')]
patients_samples_tmp <-patients_samples_tmp[(is.element(paste(patients_samples_tmp,"1",sep='_'),dimnames(total_tred_df)[[2]]))]
patients_samples_tmp<-patients_samples_tmp[!duplicated(patients_samples_tmp)]

ctrls_samples_tmp <- (pheno_df$sample[pheno_df$pheno=='EE_parents' ])
ctrls_samples_tmp <-ctrls_samples_tmp[is.element(paste(ctrls_samples_tmp,"1",sep='_'),dimnames(total_tred_df)[[2]])]
ctrls_samples_tmp<-ctrls_samples_tmp[!duplicated(ctrls_samples_tmp)]

adni_samples_tmp <- (str_replace(pheno_df$sample[pheno_df$pheno=='ADNI' ],'-','.'))
adni_samples_tmp <-adni_samples_tmp[is.element(paste(adni_samples_tmp,"1",sep='_'),dimnames(total_tred_df)[[2]])]
adni_samples_tmp<-adni_samples_tmp[!duplicated(adni_samples_tmp)]

samples_vec<-c(patients_samples_tmp,ctrls_samples_tmp,adni_samples_tmp)

## Make permutations
p<-1
str_lst<-list()
for (p in c(1:1000)) {
	samples2_vec<-sample(samples_vec)

	patients_samples2_tmp<-samples2_vec[c(1:length(patients_samples_tmp))]
	ctrls_samples2_tmp<-samples2_vec[seq(length(patients_samples_tmp)+1,length(patients_samples_tmp)+length(ctrls_samples_tmp),1)]
	adni_samples2_tmp<-samples2_vec[seq(length(patients_samples_tmp)+length(ctrls_samples_tmp)+1,length(samples2_vec),1)]

	patients_samples<-c(paste(patients_samples2_tmp,'1',sep='_'),paste(patients_samples2_tmp,'2',sep='_'))
	ctrls_samples<-c(paste(ctrls_samples2_tmp,'1',sep='_'),paste(ctrls_samples2_tmp,'2',sep='_'))
	adni_samples<-c(paste(adni_samples2_tmp,'1',sep='_'),paste(adni_samples2_tmp,'2',sep='_'))


	## Calculate IQR in fake controls
	iqr_df<-total_tred_df[,is.element(dimnames(total_tred_df)[[2]], ctrls_samples) | is.element(dimnames(total_tred_df)[[2]], adni_samples)]
	iqr_df$iqr <- apply(iqr_df,1,function(x) (IQR(x,na.rm=TRUE)))
	iqr_df$q75 <- apply(iqr_df[,-length(iqr_df[1,])],1,function(x) (quantile(x,na.rm=TRUE)))[4,]

	## Calculate IQR above q75 in fake patients
	iqr_pat_df<-total_tred_df[,is.element(dimnames(total_tred_df)[[2]], patients_samples) ]

	pat_IQR_lst=list()
	for (i in c(1:length(iqr_pat_df[1,]))){

		pat_IQR_lst[[i]] <- (iqr_pat_df[,i] - iqr_df$q75) / iqr_df$iqr
		pat_IQR_lst[[i]][iqr_df$iqr==0] <- (iqr_pat_df[,i][iqr_df$iqr==0] - iqr_df$q75[iqr_df$iqr==0]) 

	}

	iqr_pat_mat<-as.data.frame(matrix(unlist(pat_IQR_lst),nrow=length(iqr_df[,1])))
	dimnames(iqr_pat_mat)[[1]] <- dimnames(iqr_df)[[1]]
	dimnames(iqr_pat_mat)[[2]] <- dimnames(iqr_pat_df)[[2]]
	iqr_pat_mat$max<-apply(iqr_pat_mat,1,function (x) (max(x[!is.na(x)])))

	## IQR threshold
	seuil_tred_df<-total_tred_df[is.element(dimnames(total_tred_df)[[1]],dimnames(iqr_pat_mat)[[1]][iqr_pat_mat$max>seuil]),is.element(dimnames(total_tred_df)[[2]], ctrls_samples) | is.element(dimnames(total_tred_df)[[2]], patients_samples)  | is.element(dimnames(total_tred_df)[[2]], adni_samples)]

	rep_df<-seuil_tred_df %>% gather(sample,rep,dimnames(seuil_tred_df)[[2]][1]:dimnames(seuil_tred_df)[[2]][length(seuil_tred_df[1,])])
	rep_df$str<-as.factor((rep(dimnames(seuil_tred_df)[[1]],length(seuil_tred_df[1,]))))
	tmp_vec<-rep ('NA',length(rep_df[,1]))
	tmp_vec[is.element(rep_df$sample,patients_samples)] <- 'cases'
	tmp_vec[is.element(rep_df$sample,ctrls_samples)] <- 'controls'
	tmp_vec[is.element(rep_df$sample,adni_samples)] <- 'ADNI_controls'
	rep_df$group<-as.factor(tmp_vec)
	rep_df<-rep_df[rep_df$group!="NA",]

	max_df<-merge (as.data.frame(aggregate(rep~str, rep_df[rep_df$group=='cases',], FUN=max)),as.data.frame(aggregate(rep~str, rep_df[rep_df$group!='cases',], FUN=max)),by='str')
	max_df$diff<-max_df[,3]/max_df[,2]

	## Take only STRs with highest ctrl / highest patient ratio < 0.6
	seuil_max<-0.6
	rep_df<-rep_df[max_df$diff<seuil_max,]

	res_vec<-as.vector(rep_df$str[!duplicated(rep_df$str)])
	if (length(res_vec)==0){
		res_vec<-'NA'
	}
	str_lst[[p]]<-res_vec
}

## Write file
myfile <- paste(output_file,'_missing',missing_tresh,'_iqr_thresh',seuil,'_maxrepCaseCtrl',seuil_max,'_single.highest.IQR.str.rep.table_permutation.txt',sep="")
for (p in c(1:length(str_lst))){
	my_length<-length(str_lst[[p]])
	if (my_length==0){
		my_length<-1
	}
	write(str_lst[[p]],myfile,append=TRUE,ncolumns=my_length)

}


q()



